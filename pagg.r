#!/usr/bin/env Rscript

# Plot for each time slice the proportion of implied fullerene function of the
# aggregate size.
#
# Proportions are displayed as a greyscale with white mean 0% ans black more
# than 50%. This representation is heavily inspired from 
#
# Chi-cheng Chiu, Russell DeVane, Michael L. Klein, Wataru Shinoda,
# Preston B. Moore and Steven O. Nielsen
# Coarse-Grained Potential Models for Phenyl-Based Molecules: II. Application
# to Fullerenes
# J. Phys. Chem. B, 2010, 114 (19), pp 6394–6400
#
# The script reads the file given as first argument which have to be the
# frequence output of g_aggregate and write the pdf file given as second
# argument. The third argument is the number of time step per time slice.


# Most functions come from the plot.r file that needs to be "sourced". Here we
# assume that plot.r is in the same directory as the current script. If is not
# true, then you should edit the PLOT_PATH variable to make it point to the
# absolute path of the plot.r file. This part of the code is taken from
# <http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script>
initial.options <- commandArgs(trailingOnly = FALSE)
file.arg.name <- "--file="
script.name <- sub(file.arg.name, "", initial.options[grep(file.arg.name, initial.options)])
script.basename <- dirname(script.name)
PLOT_PATH <- paste(sep="/", script.basename, "plot.r")

source(PLOT_PATH)
library(shape)

# Get the command line arguments
args = commandArgs(trailingOnly = TRUE)
if (length(args) < 3 || '-h' %in% args || '--help' %in% args) {
    cat("\nUsage: pagg.r frequencies.xvg output.pdf averaging_steps [time_step [ymax]]\n\n")
    quit('no')
}
infile = args[1]
outfile = args[2]
steps = as.numeric(args[3])
if (length(args) > 3) {
    scale = as.numeric(args[4])
} else {
    scale = 0
}
if (length(args) > 4) {
    ymax = as.numeric(args[5])
} else {
    ymax = 0
}

format = strsplit(outfile, '.', fixed=TRUE)[[1]][2]

# Read the input
freq = read_frequence(infile)

# Draw the plot
if (format == "pdf") {
    #cairo_pdf(outfile)
    pdf(outfile)
} else {
    png(outfile)
}
par(mar=c(5, 4, 1, 5) + 0.1, cex=1.1, cex.lab=1.5)
plot_aggregate(freq, steps, scale=scale, ymax=ymax)
par(cex=1.1)
colorlegend(zlim=c(0,0.5), col=mygray.color()[1:50], digit=1, posx=c(0.90, 0.93))
dev.off()

