#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/** Read a point sample from a file
 *
 * File format is "%6.3f %6.3f %6.3f\n" for x, y and z.
 */
double **readPoints(FILE *infile, int ndim, int npoints);

/** Compute the distance between two points */
double distance(double *a, double *b, int ndim);
/** Compute distance matrix between points */
double **distanceMat(double **points, int ndim, int npoints);

/** Private. Iteration of the recursive algorithm */
void recursiveIteration(double threshold, double **distmat, int *clusters,
	int x, int group, int n_clusters);
/** Recursive clustering */
int *recursiveClust(double **distmat, int npoints);

/** Write the custers */

/** Run a test */
int run(double threshold, char *filename);

