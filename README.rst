===================================================
g_aggregate: Aggregate size distribution along time
===================================================

``g_aggregate`` identify molecules that aggregate and compute the aggregate
size distribution for each frame of a `GROMACS <http://www.gromacs.org>`_
trajectory. Aggregates are defined by a distance criterion: if the distance
between two molecules is shorter than a given cut-off, then the two molecules
are part of the same aggregate. Also, if two molecules are in the same
aggregate as a third molecule, they are in the same aggregate.

This program has been written by Jonathan Barnoud <jonathan.barnoud@inserm.fr>.
It is based on GROMACS source code and is distributed under the terms
of the General Public Licence version 2 or greater. See the LICENCE file for
more information.


If you use this program, please cite

::

    Lipid Membranes as Solvents for Carbon Nanoparticles
    Phys. Rev. Lett. 112, 068102 – Published 12 February 2014
    Jonathan Barnoud, Giulia Rossi, and Luca Monticelli

The citation is available in bibtex and RIS format in the ``citation``
directory.

Installation
============

``g_aggregate`` depends on the GROMACS software package, which needs to be
installed.  Only versions 4.5.x have been tested, but ``g_aggregate`` might be
compatible with other versions of GROMACS.

To install ``g_aggregate``, GROMACS needs to be loaded. You can load
it using:

    source /path_to_gromacs/bin/GMXRC

Go into the source directory of the program, then run ``make``. The
``g_aggregate`` executable should be created.  Make sure that
this executable is in the research path of your shell.

Usage
=====

Here we assume that ``g_aggregate`` is in the research path of your shell. To get
some help just run ``g_aggregate -h``. All available options will be listed.

A classical use would be:

    g_aggregate -f traj.xtc -s topol.tpr -n index.ndx -dist 1.3a -split mol

GROMACS needs to be loaded for ``g_aggregate`` to work.


Required arguments
------------------

Arguments to select the input file are mandatory. They are:

* ``-f`` to select the trajectory;
* ``-s`` to select a run file (TPR);
* ``-n`` to select an index file; if the ``-n`` option is not used, then the
  program will look for a file called ``index.ndx``.

See the section on `Group selection`_ to know what the index file should
contain.

The ``-dist`` argument is also required for the program to run. This argument
specifies the distance threshold under which two molecules are part of the same
aggregate. This argument in given in nm. By default, the distance between
center of mass is used. The minimum distance can be used by setting ``-dmat``
to ``min`` instead of ``com``.

Output control
--------------

Several files are produced by the program.

The main one---that is called ``frequencies.xvg`` by default---contains the
number of aggregate of each size on each frame. The file contains one line per
frame. The first column is the timecode in ps. The following columns are the
number of aggregate for a given size: the first column after the time column
is the number of monomers, the second column is the number of dimers, etc...
The name of this file can be changed using the option ``-of``.

An other file---called ``clusters.xvg`` bu default--- gives a group number for
each molecule on each frame. This file can be used to see that one given
molecule is in an aggregate with one other given molecule. The group numbers
are *not* consistent from one frame to the other so they cannot be used
directly to track aggregates. The name of this file can be changed using the
option ``-oc``.

Some statistics are available in the file called ``statistics.xvg``. In this
file are reported the minimum, maximum, and average aggregate size as a
function of time so as the number of aggregates. Note that, in this context, a
monomers is considered as an aggregate of size one and is counted in the number
of aggregates. The name of this file can be changed using the option ``-os``.

Group selection
---------------

There are several ways to select the groups to consider in the analysis. The
easiest ways is to have a group in your index file with all the molecules you
want to consider and to split this group using the ``-split`` option. This
group is likely to be one of the default groups written by ``make_ndx``. The
arguments for the ``-split`` option can be ``no`` (default) if you do not want
an automatic split of your group, ``atom`` if you want to consider every atom
separately, ``res`` if you want tu split the group by residues, or ``mol`` if
you want to split the group by molecules.

An other way to select the groups of interest is to give one group per object
to consider. Then you should use the ``-ng`` option to specify how many groups
will be selected.

Other options
-------------

Some extra features are available through options. Note that these feature may
be undertested.

The ``-N`` option asks the program to identify objects that have a given number
of neighbors. The result is written in a file called ``neighbors.xvg``. A
``centers.xvg`` file is also written that list the objects with the requested
number of neighbors at each frame. The name of these two files cannot be
changed in this version of the software.

Using the ``-d`` option, the distances are computed in 2D rather than in 3D. The argument of the ``-d`` option can be ``x``, ``y``, or ``z``. It is the dimension normal to the plane on which the objects will be projected.

Two algorithms are implemented in the program. Both should give the same aggregates and the same distributions (the order of the aggregates in ``clusters.xvg`` may be different). You can switch from one algorithm to the other using the ``-algo`` option. The first algorithm is a recursive algorithm. This algorithm is used with ``-algo 0``, and is the default algorithm as it is the faster one. Setting ``-algo 1``, the program will use an iterative algorithm described in [1]_



Graphical output with pagg.r
============================

A graphical output, similar to what can be found in [1]_ and [2]_, can be produced using the ``pagg.r`` script distributed with ``g_aggregate``.

The plot is a grid. The grayscale in each cell represent the fraction of objects involved in an aggregate of a given size (y-axis) at a given time (x-axis). White means 0 and black means 0.5 or more.

The script requires `R <http://www.r-project.org/>`_ to be installed so as the R package `shape <http://cran.r-project.org/web/packages/shape/index.html>`_. The shape package can be installed from within R with the following commend:

::

    install.packages("shape")


The ``pagg.r`` script is invoked like:

::

    pagg.r frequencies.xvg output.pdf averaging_steps [time_step [ymax]]

The first argument is the ``frequencies.xvg`` file produced by ``g_aggregate``
(the name may be different if you used the ``-of`` option). The second argument
is the output file. The script can produce PDF and PNG file, the format will be
chosen based on the extension. The third argument is the number of frames to
average on. The averaging is *not* a running average. The two last arguments
are optionals. ``time_step`` is the timestep (in µs) to display on the x-axis.
``ymax`` is the biggest aggregate size to display on the y-axis.


.. [1] Chi-cheng Chiu, Russell DeVane, Michael L. Klein, Wataru Shinoda, Preston B. Moore and Steven O. Nielsen
    Coarse-Grained Potential Models for Phenyl-Based Molecules: II. Application to Fullerenes
    J. Phys. Chem. B, 2010, 114 (19), pp 6394–6400

.. [2] Luca Monticelli, Jonathan Barnoud, Adam Orlowski, Ilpo Vattulainen
    Interaction of C 70 fullerene with the Kv1. 2 potassium channel
    Phys. Chem. Chem. Phys., 2012, 36 (14), pp 12526-12533


