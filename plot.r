read_frequence = function(path) {
    return(read.table(path, skip=8, comment.char="@", header=FALSE))
}

max_cluster_size = function(freqmat) {
    a = freqmat
    #return(max(which(sapply(2:ncol(a), function(x) all(a[,x] == 0)) == FALSE)))
    return(ncol(freqmat)-1)
    #return(40)
}

create_empty_matrix = function(freqmat, nsteps) {
    return(matrix(-1, nrow=max_cluster_size(freqmat),
                     ncol=floor(nrow(freqmat)/nsteps)))
}

fill_matrix = function(freqmat, nsteps) {
    max_size = max_cluster_size(freqmat)
    effectifs = create_empty_matrix(freqmat, nsteps)
    print(dim(effectifs))
    for (stepi in 1:(floor(nrow(freqmat)/nsteps))) {
        step_index = ((stepi-1)*nsteps)+1
        effectifs[,stepi] = sapply(1:max_size,
            function(size) sum(freqmat[step_index:(step_index+nsteps-1), size+1])
        )
    }
    for (stepi in 1:ncol(effectifs)) {
        effectifs[,stepi] = effectifs[,stepi]*seq(1:nrow(effectifs))
        effectifs[,stepi] = 100*effectifs[,stepi]/sum(effectifs[,stepi])
    }
    return(effectifs)
}

mygray = function(x) {
    if (x > 0.5) return(gray(1))
    #print(1-(x*2))
    return(gray(1-(x*2)))
}

mygray.color = function() {
    a = rep("#000000", 100)
    a[1] = "#ffffff"
    a[2] = "#ffffff"
    for (i in seq(2, 50)) {
        a[i+1] = mygray((i)/100)
        print(c(1-(i)/100, i, i/100, a[i]))
    }
    return(a)
}

plot_aggregate = function(freqmat, nsteps, main="", scale=0, ymax=0) {
    mat = fill_matrix(freqmat, nsteps)
    if (ymax == 0) {
        ymax=max_cluster_size(freqmat)
    }
    image(t(mat)[,1:ymax], col=mygray.color(), axes=FALSE)
    #axis(1,seq(0,1,length.out=ncol(mat)/10),
    #     round(seq(0,freqmat[nrow(freqmat),1]/1000000,
    #               length.out=ncol(mat)/10),2))
    if (scale == 0) {
        axis(1,seq(0,1,length.out=11),
             round(seq(0,freqmat[nrow(freqmat),1]/1000000,
                   length.out=11),2))
    } else {
        axis(1, seq(0,1,by=(scale*1000000/(freqmat[nrow(freqmat),1]))), seq(0,freqmat[nrow(freqmat),1], by=scale*1000000)/1000000)
    }
    axis(2,seq(0,1,length.out=ymax),
         1:ymax)
    title(xlab="Time (microseconds)", ylab="Aggregate size", main=main)
    box()
}
