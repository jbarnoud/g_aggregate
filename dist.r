#!/usr/bin/env Rscript

# !!! This path have to be edited to reflect the real location of plot.r !!!
PLOT_PATH = "~/dev/g_aggregate/plot.r"

source(PLOT_PATH)

get_dist = function(freqmat, begin=0, last=-1){
    if (last == -1) {
        last=freqmat[dim(freqmat)[1],1]
    }
    freq = freqmat[freqmat[,1]>=begin,]
    freq = freq[freq[,1]<=last,-1]
    dist = apply(freq, 2, sum)*1:ncol(freq)
    dist = dist[1:max(which(dist > 0))]
    dist = dist/prod(dim(freq))
    return(dist)
}


# Get the command line arguments
args = commandArgs(trailingOnly = TRUE)
infile = args[1]
outfile = args[2]

begin = 0
last = -1
if (length(args) >= 3) {
    begin = as.numeric(args[3])
}
if (length(args) >= 4) {
    last = as.numeric(args[4])
}

format = strsplit(outfile, '.', fixed=TRUE)[[1]][2]

dist = get_dist(read_frequence(infile), begin, last)*100

# Write the distribution on the standard output in xvg format
for (i in 1:length(dist)) {
    cat(sprintf("%d  %12.7f\n", i, dist[i]))
}
    

# Draw the plot
if (format == "pdf") {
pdf(outfile)
} else {
png(outfile)
}



#print(sum(1:length(dist)*dist)/sum(dist))

plot(1:length(dist), dist, main="Aggregate size distribution", xlab="Aggregate size", ylab="Population proportion (%)", ylim=c(0,max(dist)+2), type="b")
text(1:length(dist), dist+2, as.character(round(dist, 2)))
#print(dist[1])

silent = dev.off()

