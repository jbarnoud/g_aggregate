#include "float.h"
#include "limits.h"
#include "math.h"
#include "ctype.h"
#include "string.h"

#include <gromacs/copyrite.h>
#include <gromacs/macros.h>
#include <gromacs/pbc.h>
#include <gromacs/rmpbc.h>
#include <gromacs/smalloc.h>
#include <gromacs/statutil.h>
#include <gromacs/vec.h>
#include <gromacs/xvgr.h>

/******************************************************************************
 * Display credits
 *****************************************************************************/

static const char *authors[] = {
    "Written by Jonathan Barnoud (jonathan.barnoud@inserm.fr)",
    "Copyright (c) 2012  Jonathan Barnoud, Luca Monticelli"
};
static const char *gpl[] = {
    "This program is free software; you can redistribute it and/or",
    "modify it under the terms of the GNU General Public License",
    "(version 2) as published by the Free Software Foundation."
};
void sp_print(FILE *out, const char *txt)
{
    int i, s;
    s = (int) (80 - strlen(txt)) / 2.0;
    for (i=0; i < s; i++) 
        fprintf(out, " ");
    fprintf(out, "%s\n", txt);
}

/******************************************************************************
 * Split groups
 *****************************************************************************/

/*
 * The program needs to now which atoms to consider so the user provides groups
 * using an index file. The user can choose, either one index group per
 * particlethat forms a unit in the clustering, or one group that will be
 * splitted into units based on molecules, residues or atoms.
 *
 * Index groups, as read per gromacs, are represented by three tables:
 * - a table that contains the number of atoms in each group, this table is
 *   usualy called isize
 * - a table that contains a table per group which contains the indices of the
 *   selected atoms in the global lists of atoms, this table is usually called
 *   index
 * - a table that contains the name of each group as a string, usually called
 *   grpname
 *
 * The function to split a group (split_group) has been borrowed from gmx_rdf;
 * it returns two
 * values:
 * - the number of groups as an integer (called is or is_out)
 * - a table that contains the first index of each group after splitting. The
 *   size of this table is (is +1); the last item is the index of the last
 *   index + 1.
 * 
 * The splitted_to_regular function convert the format outputed by split_group
 * into the regular format for index groups. 
 */

/* Modified from gmx_rdf */
static void split_group(int isize,int *index,char *grpname,
        t_topology *top,char type,
        int *is_out,int **coi_out)
{
    t_block *mols=NULL;
    t_atom  *atom=NULL;
    int     is,*coi;
    int     cur,mol,res,i,a,i1;
    char type_long[10];

    /* Split up the group in molecules or residues */
    switch (type) {
        case 'm':
            mols = &top->mols;
            strcpy(type_long, "molecules\0");
            break;
        case 'r':
            atom = top->atoms.atom;
            strcpy(type_long, "residues\0");
            break;
        case 'n':
            strcpy(type_long, "group\0");
            break;
        case 'a':
            strcpy(type_long, "atoms\0");
            break;
        default:
            gmx_fatal(FARGS,"Unknown splitting option '%s'",type);
    }
    snew(coi,isize+1);
    is = 0;
    cur = -1;
    mol = 0;
    if (type == 'n') {
        coi[0] = 0;
        coi[1] = isize;
        is++;
    }
    else {
        for(i=0; i<isize; i++) {
            a = index[i];
            if (type == 'm') {
                /* Check if the molecule number has changed */
                i1 = mols->index[mol+1];
                while(a >= i1) {
                    mol++;
                    i1 = mols->index[mol+1];
                }
                if (mol != cur) {
                    coi[is++] = i;
                    cur = mol;
                }
            } else if (type == 'r') {
                /* Check if the residue index has changed */
                res = atom[a].resind;
                if (res != cur) {
                    coi[is++] = i;
                    cur = res;
                }
            } else if (type == 'a') {
                coi[is++] = i;
            }
        }
    coi[is] = i;
    }
    srenew(coi,is+1);
    printf("Group '%s' of %d atoms consists of %d %s\n",
            grpname,isize,is,type_long);

    *is_out  = is;
    *coi_out = coi;
}

/* Transform splitted groups into regular ones */
void splitted_to_regular(int ngroups, int *is, int **coi, int **index,
        int ***out_index, int **out_isize, int *nsubgroups) {
    int atom, group, subgroup, idx=0;

    *nsubgroups = 0;

    /* How many groups in total ? */
    for (group=0; group<ngroups; ++group) {
        *nsubgroups += is[group];
    }

    /* Allocate result storage */
    snew((*out_index), *nsubgroups);
    snew((*out_isize), *nsubgroups);

    /* Write one group per subgroup */
    idx = 0;
    for (group=0; group<ngroups; ++group) {
        for (subgroup=0; subgroup<is[group]; ++subgroup) {
            /* Get the size of the group */
            (*out_isize)[idx] = coi[group][subgroup+1] - coi[group][subgroup];
            snew((*out_index)[idx], (*out_isize)[idx]);
            /* Write the atoms */
            for (atom=0; atom<(*out_isize)[idx]; ++atom) {
                (*out_index)[idx][atom] = index[group][coi[group][subgroup] + \
                                          atom];
            }
            idx++;
        }
    }
}
        
/******************************************************************************
 *  Handle data structures
 *****************************************************************************/

/** Create a matrix of real numbers
 *
 * The matrix is pre-filled with a value.
 *
 * Parameters:
 *  - d1: number of rows
 *  - d2: number of columns
 *  - defval: the value to put in every cells
 *
 * Return:
 *  The filled matrix.
 */
real **realMatrix(int d1, int d2, real defval){
    int i, j;
    real **mat;
  
    smalloc(mat, d1 * sizeof(real*));
    for(i = 0; i < d1; i++){
        smalloc(mat[i], d2 * sizeof(real));
        for(j = 0; j < d2; j++) {
            mat[i][j] = defval;
        }
    }
    return mat;
}

/** Destroy a matrix of real numbers
 *
 * Parameters:
 *  - mat: the matrix to destroy
 *  - d1: the number of rows in the matrix
 */
void deleteRealMat(real **mat, int d1) {
    int i;
    for(i = 0; i < d1; i++) {
        sfree(mat[i]);
    }
    sfree(mat);
}

/******************************************************************************
 *  Clustering
 *****************************************************************************/

/*
 * Recursive algorithm
 * ===================
 *
 * The recursive algorithm is considering a distance matrix as an adjacency
 * matrix. An edge is considered if the distance is bellow a given threshold.
 *
 * The clustering is made by the labeling of each connected component of the
 * graph.
 */

/** Label recursively the vertices in the same connected component
 *
 * Parameters:
 *  - distmat: the matrix distance to use as graph adjacency matrix
 *  - threshold: maximum distance to consider an edge
 *  - npoints: number of vertices
 *  - position: current root for the component
 *  - group: component label
 *  - clusters: table storing the label for each vertex
 *  - nclusters: number of found connected component
 */
void recursiv_it(real **distmat, real threshold, int npoints,
  int position, int group, int *clusters, int *nclusters) {
    int i;
    clusters[position] = group;
    for (i=0; i<npoints; i++) {
        if (distmat[position][i] < threshold && i != position && !clusters[i]) {
            if (group == 0) {
                (*nclusters)++;
                group = (*nclusters);
                clusters[position] = group;
            }
            recursiv_it(distmat, threshold, npoints, i, group,
              clusters, nclusters);
        }
    }
}

int *recursiv_clust(real **distmat, real threshold, int npoints) {
    int *clusters;
    int nclusters = 0;
    int i;

    scalloc(clusters, npoints, sizeof(int));

    for (i=0; i<npoints; i++) {
        if (!clusters[i]) {
            recursiv_it(distmat, threshold, npoints,
                i, 0, clusters, &nclusters);
        }
    }

    /* At this point, all groups bigger than one element are labelled properly
     * but all monomers are labelled 0. This could be convenient but it makes
     * statistics more difficult. Let's give a proper label for each monomer.
     */
    /* The first available cluster is nclusters + 1*/
    for (i=0; i<npoints; i++) {
        if (clusters[i] == 0) {
            nclusters++;
            clusters[i] = nclusters;
        }
    }

    return clusters;
}

/*
 * Iterative algorithm
 * ===================
 *
 * Algorithm used in 
 * Coarse-grained potential models for phenyl-based molecules:
 * II. Application to fullerenes
 * Chi-cheng Chiu, Russell DeVane, Michael L Klein, Wataru Shinoda,
 * Preston B Moore, Steven O Nielsen (2010)
 * The Journal of Physical Chemistry. B 114 (19) p. 6394-6400 
 */

int *iterativ_clust(real **distmat, real threshold, int npoints) {
    int *clusters;
    real mindist = GMX_REAL_MAX;
    int minindex = 0;
    int index = 0;
    int ngroups = 1;
    int i, j, viewed;
    scalloc(clusters, npoints, sizeof(int));
    clusters[0] = ngroups;
    for (viewed=1; viewed<npoints; viewed++) {
        index = minindex;
        mindist = DBL_MAX;
        // Find the closest point
        for (i=0; i<npoints; i++) {
            if (clusters[i] == clusters[index]) {
                for (j=0; j<npoints; j++) {
                    if (!clusters[j] && distmat[i][j] < mindist) {
                        minindex = j;
                        mindist = distmat[i][j];
                    }
                }
            }
        }
        // Is the closest point in the cluster ?
        if (mindist > threshold) {
            ngroups++;
        }
        clusters[minindex] = ngroups;
    }
    return clusters;
}

/******************************************************************************
 *  Distance matrix calculation
 *****************************************************************************/

real *computeMass(t_topology *top, int ngrps, int *isize, atom_id **index) {
    real *mass;
    snew(mass,ngrps);
    int g,i;
    for(g=0; g<ngrps; g++) {
        mass[g]=0;
        for(i=0; i<isize[g]; i++) {
            mass[g]+=top->atoms.atom[index[g][i]].m;
        }
    }
    return mass;
}

rvec *massCenter(t_topology *top, rvec *x, int ngrps, atom_id **index,
  int *isize, real *mass) {
    rvec *centers;
    int g,d,i;
    snew(centers, ngrps);
    for(g=0; g<ngrps; g++) {
        if (isize[g] == 1) {
	        copy_rvec(x[index[g][0]], centers[g]);
        }
        else {
	        for(d=0; d<DIM; d++) {
	            centers[g][d]=0;
	            for(i=0; i<isize[g]; i++) {
	                centers[g][d] += x[index[g][i]][d]
	                  * top->atoms.atom[index[g][i]].m;
	            }
	        centers[g][d] /= mass[g];
	        }
	    }
	}
	return centers;
}

void distanceMat(real **distmat, rvec *x, int npoints, t_pbc   *pbc) {
	real dist;
	rvec dx;
    int i,j;
	for (i=1; i < npoints; i++) {
	    for (j=0; j<i; j++) {
	        if (pbc)
	            pbc_dx(pbc,x[i],x[j],dx);
	        else
	            rvec_sub(x[i], x[j], dx);
            dist = sqrtf(norm2(dx));
	        distmat[i][j] = dist;
	        distmat[j][i] = dist;
	    }
	}
}

/** Build a distance matrix based on the minimum distance between groups
 */
void minDistMat(real **distmat, rvec *x, int ngroups,
        int **index, int *isize, t_pbc *pbc) {
    real mindist, dist;
    rvec dx;
    int i,j,atom_i,atom_j;
    for (i=0; i < ngroups; i++) {
        for (j=0; j<i; j++) {
            mindist = DBL_MAX;
            for (atom_i=0; atom_i<isize[i]; atom_i++) {
                for (atom_j=0; atom_j<isize[j]; atom_j++) {
                    if (pbc) {
                        pbc_dx(pbc,x[index[i][atom_i]],x[index[j][atom_j]],dx);
                    }
                    else {
                        rvec_sub(x[index[i][atom_i]],x[index[j][atom_j]], dx);
                    }
                    dist = norm2(dx);
                    if (dist < mindist) {
                        mindist = dist;
                    }
                }
            }
            mindist = sqrtf(mindist);
            distmat[i][j] = mindist;
            distmat[j][i] = mindist;
            //printf("(i, j) : (%d, %d), mindist : %f\n", i, j, mindist);
        }
    }
}



rvec *switch2D(rvec *x, int npoints, int axis) {
    int i;
    rvec *x2D;
    snew(x2D, npoints);
    copy_rvecn(x, x2D, 0, npoints);
    for (i=0; i<npoints; i++) {
        x2D[i][axis] = 0;
    }
    return x2D;
}       

/******************************************************************************
 *  Properties measurement
 *****************************************************************************/

void clusterSizes(int *sizes, int *freq, int *clusters, int ngrps, int *max,
  int *min, real *avg, int *nclust) {
    int i, sum=0;
    *nclust = 0;
    *max = 0;
    *min = ngrps;
    for (i=0; i<ngrps; i++) {
        sizes[i] = 0;
        freq[i] = 0;
    }
    // Count the number of groups in each cluster
    for (i=0; i<ngrps; i++) {
        if (clusters[i] == 0) {
            freq[0]++;
        }
        else {
            sizes[clusters[i]-1]++;
        }
    }
    // Count the number of clusters for each size
    for (i=0; i<ngrps; i++) {
        if (sizes[i] > 0) {
            freq[sizes[i]-1]++;
        }
    }
    // Find the biggest, smallest and averaged aggregate size
    for (i=0; i<ngrps; i++) {
        if (sizes[i] > *max) {
            *max = sizes[i];
        }
        else if (sizes[i] < *min && sizes[i] > 0) {
            *min = sizes[i];
        }
        sum += ((i+1)*freq[i]);
        (*nclust) += freq[i];
    }
    *avg = (real)sum/(real)(*nclust);
}

/** Build a table with all the group index in a given cluster */
int *clusterIndex(int *clusters, int clusterId, int size, int ngrps) {
    int group=0, i=0;
    int *index;
    snew(index, size);
    for (group=0; group<ngrps; group++) {
        if (clusters[group] == clusterId) {
            index[i] = group;
            i++;
        }
    }
    return index;
}

/** Count the number of groups with a given number of neighbors */
void findGeometry(real **distmat, int *sizes, int *clusters,
  int ngrps, int neighbors, real dist, FILE *output, real time, FILE *output2) {
    int i, j, cluster, *index=NULL, nNeighbors, match=0;
    fprintf(output2, "%12.7f ", time);
    for (cluster=0; cluster<ngrps; cluster++) {
        if (sizes[cluster] >= neighbors) {
            index = clusterIndex(clusters, cluster+1, sizes[cluster], ngrps);
            for (i=0; i<sizes[cluster]; i++) {
                printf("%d ", index[i]);
            }
            printf("\n");
            for (i=0; i<sizes[cluster]; i++) {
                nNeighbors=0;
                for (j=0; j<sizes[cluster]; j++) {
                    if (i != j && distmat[index[i]][index[j]] <= dist) {
                        nNeighbors++;
                    }
                }
                if (nNeighbors == neighbors) {
                    match++;
                    fprintf(output2, "%d ", i);
                }
            }
            sfree(index);
        }
    }
    fprintf(output2, "\n");
    fprintf(output, "%12.7f %d\n", time, match);
} 

/******************************************************************************
 * Main work
 *****************************************************************************/

int main(int argc, char **argv) {
    const char *desc[] = {
        "g_aggregate finds clusters of aggregated molecules.",
        "Clustering is based on a distance criterion, specified with the",
        "[TT]-dist[tt] parameter.",
    };
    int i;
	real **distmat = NULL;
	int *clusters = NULL;
	static real threshold;
	static int algo = 0;

	/* frame reading */
    output_env_t oenv;
    int natoms;
    t_trxstatus *status;
    real t;
    rvec *x=NULL;
    rvec *usedx=NULL;

    /* index reading */
    static int ngrps = 1;
    char    **grpname; /* the name of each group */
    int     *isize;    /* the size of each group */
    atom_id **index;   /* the index for the atom numbers */
    t_topology *top=NULL;
    int *is_out = NULL, **coi_out=NULL;
    int **sindex = NULL;
    int *sisize = NULL;
    int nsubgroups;

    /* periodic conditions */
    int  ePBC;
    t_pbc   *pbc;
    gmx_rmpbc_t  gpbc=NULL;
    matrix box;

    real *mass = NULL;
    rvec *centers = NULL;

    /* output */
    int *sizes;
    int *frequencies = NULL;
    FILE *outFreq=NULL, *outClusters=NULL, *outStats=NULL, *outNeighbors=NULL;
    FILE *outCenters=NULL;
    char **leg;
    int nclust=0;
    int itSizeMax = 0, sizeMax = 0, itSizeMin=0;
    real itSizeAvg=0.0;
    const char legTemplate[][15] = {
        "monomers\0",
        "dimers\0",
        "trimers\0",
        "tetramers\0",
        "pentamers\0",
        "hexamers\0",
        "heptamers\0",
        "octamers\0",
        "nonamers\0",
        "decamers\0",
    };
    const char statLegendsTemplate[][15] = {
        "minimum\0",
        "maximum\0",
        "average\0",
        "# clusters\0",
    };
    char **statLegends = NULL;
	
	/* parameters */
    static const char *split[]={ NULL, "no", "atom", "mol", "res", NULL };
    static const char *axtitle[] = { NULL,"no","x","y","z",NULL };
    static const char *dmat[] = { NULL,"com","min",NULL };
    int axis = -1;
    static int neighbors = -1;
    static t_pargs pa[] = {
        { "-dist",      FALSE, etREAL, {&threshold},
            "Molecules are aggregated if the distance between them is less "
            "than dist"},
        { "-algo",      FALSE, etINT, {&algo},
            "The algotithm to use; 0 is recursive, 1 iterative"},
        { "-ng",      FALSE, etINT, {&ngrps},
            "The number of groups to consider"},
        { "-d", FALSE, etENUM, {axtitle}, 
            "Calculate distances in 2D, in the plane normal to a given axis" },
        { "-N", FALSE, etINT, {&neighbors},
            "Count at each time step the number of groups with exactly this"
            "number of direct neighbors."},
        { "-split",   FALSE, etENUM, {split}, 
            "How to split the group" },
        { "-dmat", FALSE, etENUM, {dmat},
            "The kind of matrix distance. com for average distance between"
                " groups, min for minimum distance"},
    };
    #define NPA asize(pa)

	t_filenm fnm[] = {
        { efTRX, "-f", NULL, ffREAD },
        { efTPX, NULL, NULL, ffREAD },
        { efNDX, NULL, NULL, ffOPTRD },
        { efXVG, "-of", "frequencies", ffOPTWR },
        { efXVG, "-oc", "clusters", ffOPTWR },
        { efXVG, "-os", "statistics", ffOPTWR },
    };
    #define NFILE asize(fnm)
    
    /* Display GROMACS credit */ 
    CopyRight(stderr, argv[0]);
    
    /* Display g_aggregate credit */
    for (i=0; i < (int)asize(authors); i++)
        sp_print(stderr, authors[i]);
    fprintf(stderr, "\n");
    for (i=0; i < (int)asize(gpl); i++)
        sp_print(stderr, gpl[i]);
    fprintf(stderr, "\n");
    
    parse_common_args(&argc,argv,PCA_CAN_TIME | PCA_BE_NICE,
	    NFILE,fnm,NPA,pa,asize(desc),desc,0,NULL,&oenv);

	/* Complain if the distance threshold is <= 0 */
	if (threshold <= 0) {
	    gmx_fatal(FARGS, "-dist can not be negative or null.");
	}

    top=read_top(ftp2fn(efTPX,NFILE,fnm),&ePBC);

    /* Define axis */
    axis = toupper(axtitle[0][0]) - 'X';
    
    /* Read index file */
    snew(grpname,ngrps);
    snew(index,ngrps);
    snew(isize,ngrps);
    get_index(&top->atoms,ftp2fn(efNDX,NFILE,fnm),ngrps,isize,index,grpname);

    /* Split groups */
    snew(is_out, ngrps);
    snew(coi_out, ngrps);
    for (i=0; i<ngrps; ++i) {
        split_group(isize[i], index[i], grpname[i], top, split[0][0],
                &(is_out[i]), &(coi_out[i]));
    }
    splitted_to_regular(ngrps, is_out, coi_out, index,
            &sindex, &sisize, &nsubgroups);
    sfree(is_out);
    sfree(coi_out);

    /* Allocate the distance matrix */
    distmat = realMatrix(nsubgroups, nsubgroups, 0);

    /* Prepare output */
    scalloc(sizes, nsubgroups, sizeof(int));
    scalloc(frequencies, nsubgroups, sizeof(int));
    outFreq = xvgropen(opt2fn("-of",NFILE,fnm),
		  "Number of clusters for a size", "Time (ps)", "Effectif", oenv);
    outClusters = xvgropen(opt2fn("-oc",NFILE,fnm),
		  "Cluster for each group", "Time (ps)", "Cluster id",oenv);
    outStats = xvgropen(opt2fn("-os",NFILE,fnm),
		  "Aggregation overview", "Time (ps)",
		  "Aggregate size (number of elements)",oenv);
    snew(statLegends, 4);
    for (i=0; i<4; ++i) {
        snew(statLegends[i], 20);
        strcpy(statLegends[i], statLegendsTemplate[i]);
    }
    xvgr_legend(outStats,4,(const char**)statLegends,oenv);
    for (i=0; i<4; ++i) {
        sfree(statLegends[i]);
    }
    sfree(statLegends);
    if (neighbors > 0) {
        outNeighbors = xvgropen("neighbors.xvg",
            "Number of groups with a given number of neighbors",
            "Time (ps)", "Number of groups", oenv);
        outCenters = xvgropen("centers.xvg",
            "Group ID with the given number of neighbors",
            "Time (ps)", "Number of groups", oenv);
    }

    natoms=read_first_x(oenv,&status,ftp2fn(efTRX,NFILE,fnm),&t,&x,box);
    if (dmat[0][0] == 'c') {
        mass = computeMass(top, nsubgroups, sisize, sindex);
    }
   
    /* read the box for periodic conditions */
    if (ePBC != epbcNONE)
        snew(pbc,1);
    else
        pbc = NULL;
    
    gpbc = gmx_rmpbc_init(&top->idef,ePBC,natoms,box);


    do {
        /* initialisation for correct distance calculations */
        if (pbc) {
            set_pbc(pbc,ePBC,box);
            /* make molecules whole again */
            gmx_rmpbc(gpbc,natoms,box,x);
        }

        /* calculate distance matrix */
        if (axis < 0) {
            usedx = x;
        }
        else {
            /* 2D mode */
            usedx = switch2D(x, nsubgroups, axis);
        }
        if (dmat[0][0] == 'c') {
            centers = massCenter(top, usedx, nsubgroups, sindex, sisize, mass);
            distanceMat(distmat, centers, nsubgroups, pbc);
        }
        else {
            minDistMat(distmat, usedx, nsubgroups, sindex, sisize, pbc);
        }
        if (axis >= 0) {
            sfree(usedx);
        }
        sfree(centers);
        
        /* do clustering */
        if (algo == 0) {
            clusters = recursiv_clust(distmat, threshold, nsubgroups);
        }
        else if (algo == 1) {
            clusters = iterativ_clust(distmat, threshold, nsubgroups);
        }
        
        /* cluster output */
        fprintf(outClusters,"%12.7f ",t);
        for (i=0; i<nsubgroups; i++) {
            fprintf(outClusters, "%i ", clusters[i]);
        }
        fprintf(outClusters, "\n");
        
        /* statistics */
        clusterSizes(sizes, frequencies, clusters, nsubgroups, &itSizeMax,
          &itSizeMin, &itSizeAvg, &nclust);
        if (itSizeMax > sizeMax) {
            sizeMax = itSizeMax;
        }

        /* frequency output */
        fprintf(outFreq,"%12.7f ",t);
        for (i=0; i<nsubgroups; i++) {
            fprintf(outFreq, "%i ", frequencies[i]);
        }
        fprintf(outFreq, "\n");
        fprintf(outStats,"%12.7f %d %d %12.7f %d\n",t, itSizeMin, itSizeMax,
          itSizeAvg, nclust);

        /* neighbor search */
        if (neighbors > 0) {
            findGeometry(distmat, sizes, clusters, nsubgroups, neighbors,
                    threshold, outNeighbors, t, outCenters);
        }
        sfree(clusters);
    } while(read_next_x(oenv,status,&t,natoms,x,box));
    
    /* write legend in the frequency output */
    snew(leg, sizeMax);
    for (i=0; i<sizeMax; i++) {
        snew(leg[i], 20);
        if (i < (int)asize(legTemplate)) {
            strcpy(leg[i], legTemplate[i]);
        }
        else {
            sprintf(leg[i], "%i-meres", i+1);
        }
    }
    xvgr_legend(outFreq,sizeMax,(const char**)leg,oenv);  

    /* Cleaning */
    gmx_rmpbc_done(gpbc);
    deleteRealMat(distmat, nsubgroups);
    sfree(mass);
    sfree(sizes);
    sfree(frequencies);
    sfree(grpname);
    for (i=0; i<ngrps; ++i) {
        sfree(index[i]);
        sfree(sindex[i]);
    }
    sfree(sindex);
    sfree(sisize);
    sfree(index);
    sfree(isize);
    for (i=0; i<sizeMax; i++) {
        sfree(leg[i]);
    }
    sfree(leg);
    ffclose(outFreq);
    ffclose(outClusters);
    ffclose(outStats);
    if (outNeighbors) {
        ffclose(outNeighbors);
        ffclose(outCenters);
    }

    thanx(stderr);

	return 0;
}
		
